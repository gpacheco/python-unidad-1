def contar_letra (string, letra = 'a'):
    """
    devuelve la cantidad de apariciones de la letra en el string
    """
    cant = 0

    for x in string.lower():
        if x == letra:
            cant += 1
        
    return cant
    
        
string = str(input ("Ingrese un frase: "))

print("En la frase ingresada aparece", contar_letra(string), "veces la letra a")