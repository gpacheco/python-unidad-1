def max (nro1, nro2):
    """
    devuelve el mayor entre 2 nros.
    """
    if nro1 > nro2:
        return nro1
    
    return nro2
        
nro1 = int(input ("Ingrese un nro: "))

nro2 = int(input ("Ingrese otro nro: "))

print("El nro mayor es: ", max(nro1, nro2))
