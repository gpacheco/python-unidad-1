def contar_vocal (string):
    """
    devuelve la cantidad de apariciones de vocales en el string
    """
    cant = 0

    for x in string.lower():
        if x == 'a' or x == 'e' or x == 'i' or x == 'o' or x == 'u':
            cant += 1
        
    return cant
    
        
string = str(input ("Ingrese un frase: "))

print("En la frase tiene", contar_vocal(string), "vocales")