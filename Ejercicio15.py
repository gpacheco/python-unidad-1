def divisores_comunes (nro1, nro2):
    """
    imprime los divisores comunes de dos nros
    """
    print("Los divisores comunes del nro", nro1, " y el nro", nro2, "son:")

    if(nro1 > nro2):
        mayor = nro1
    else:
        mayor = nro2

    for x in range(mayor, 0, -1):
        if nro1 % x == 0 and nro2 % x == 0 :
            print (x)
        
        
nro1 = int(input ("Ingrese un nro: "))
nro2 = int(input ("Ingrese otro nro: "))

divisores_comunes(nro1, nro2)
