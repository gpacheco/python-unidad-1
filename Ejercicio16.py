def primo (nro):
    """
    indica si un nro es primo
    """
    
    for x in range(nro-1, 1, -1):
        if nro % x == 0 :
            return False
    
    return True
        
        
nro = int(input ("Ingrese un nro: "))

if (primo(nro)):
    print ("El nro es primo.")
else:
    print ("El nro no es primo.")
