def imprimir_vocales (string):
    """
    imprime las vocales que aparecen en la frase
    """
    
    a = False
    e = False
    i = False
    o = False
    u = False

    for x in string.lower():
        if x == 'a' and not a:
            a = True
            print ("La frase tiene la letra a.")
        else:
            if x == 'e' and not e:
                e = True
                print ("La frase tiene la letra e.")
            else:
                if x == 'i' and not i:
                    i = True
                    print ("La frase tiene la letra i.")
                else:
                    if x == 'o' and not o:
                        o = True
                        print ("La frase tiene la letra o.")
                    else:
                        if x == 'u' and not u:
                            u = True
                            print ("La frase tiene la letra u.")
    
        
string = str(input ("Ingrese un frase: "))

imprimir_vocales(string)
