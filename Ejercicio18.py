def calificacion (nota):
    """
    imprime la calificación según la nota obtenida
    """

    if 0 <= nota < 3:
        print ("Muy deficiente")
    else:
        if 3 <= nota < 5:
            print ("Insuficiente")        
        else:
            if 5 <= nota < 6:
                print ("Suficiente")
            else:
                if 6 <= nota < 7:
                    print ("Bien")
                else:
                    if 7 <= nota < 9:
                        print ("Notable")
                    else:
                        if 9 <= nota <= 10:
                            print ("Sobresaliente")

nro = int(input ("Ingrese un nro: "))

calificacion(nro)
