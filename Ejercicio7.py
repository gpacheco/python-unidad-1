def divisible (nro1, divisor = 2):
    """
    dice si un nros es divisible por el divisor.
    """
    if nro1 % divisor == 0:
        return True
    
    return False
    
        
nro1 = int(input ("Ingrese un nro: "))

if divisible(nro1):
    print("El nro", nro1, "es divisible por 2")
else:
    print("El nro", nro1, "no es divisible por 2")