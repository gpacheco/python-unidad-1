def max (nro1, nro2, nro3):
    """
    devuelve el mayor entre 3 nros.
    """
    if nro1 > nro2:
        if nro1 > nro3:
            return nro1
        else:
            return nro3
    else:
        if nro2 > nro3:
            return nro2
        else:
            return nro3
    
        
nro1 = int(input ("Ingrese un nro: "))

nro2 = int(input ("Ingrese otro nro: "))

nro3 = int(input ("Ingrese otro nro: "))

print("El nro mayor es: ", max(nro1, nro2, nro3))
