def contar_vocales (string):
    """
    imprime la cantidad de veces que aparecen las vocales en la frase
    """
    
    a = 0
    e = 0
    i = 0
    o = 0
    u = 0

    for x in string.lower():
        if x == 'a':
            a += 1
        else:
            if x == 'e':
                e += 1
            else:
                if x == 'i':
                    i += 1
                else:
                    if x == 'o':
                        o += 1
                    else:
                        if x == 'u':
                            u += 1

    print("La A aparece", a, "veces.")
    print("La E aparece", e, "veces.")
    print("La I aparece", i, "veces.")
    print("La O aparece", o, "veces.")
    print("La U aparece", u, "veces.")
        
string = str(input ("Ingrese un frase: "))

contar_vocales(string)
